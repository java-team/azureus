#!/bin/sh -e

VERSION=$2
TAR=../azureus_$VERSION.orig.tar.xz
DIR="azureus-$VERSION"
UPSTREAM_VERSION="`echo $VERSION | sed  's/\.//g'`"
TAG="RELEASE_$UPSTREAM_VERSION"
echo "Version: $VERSION"
echo "Upstream version: $UPSTREAM_VERSION"

svn export http://svn.vuze.com/public/client/tags/${TAG} $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude '*.jar' \
  --exclude '*.class' --exclude 'Tree2.java' --exclude 'azureus2/lib/*' \
  --exclude 'azureus2/src/org/json/*' --exclude 'azureus3/src/org/apache/*' \
  --exclude 'azureus2/src/org/bouncycastle/*' --exclude 'test/*' $DIR
rm -rf $DIR ../$TAG $3
